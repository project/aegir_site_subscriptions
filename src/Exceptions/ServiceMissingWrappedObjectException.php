<?php

namespace Drupal\aegir_site_subscriptions\Exceptions;

/**
 * Exception for services missing wrapped objects that were not set.
 */
abstract class ServiceMissingWrappedObjectException extends \RuntimeException {
}
