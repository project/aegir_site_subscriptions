<?php

namespace Drupal\aegir_site_subscriptions\HostingServiceCalls;

/**
 * Fetches the list of profiles via Aegir's Web service API.
 */
abstract class ListHostingServiceCall extends HostingServiceCall {

}
