<?php

namespace Drupal\aegir_site_subscriptions\Exceptions;

/**
 * Exception for remote task creation failures.
 */
class TaskCreationFailedException extends \RuntimeException {
}
