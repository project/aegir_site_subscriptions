<?php

namespace Drupal\aegir_site_subscriptions_recurly\WebhookNotificationHandlers;

/**
 * Processes Recurly notifications for Aegir.
 */
abstract class PaymentWebhookNotificationHandler extends WebhookNotificationHandler {

}
