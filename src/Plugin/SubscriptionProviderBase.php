<?php

namespace Drupal\aegir_site_subscriptions\Plugin;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for Aegir site subscription provider plugins.
 */
abstract class SubscriptionProviderBase extends PluginBase implements SubscriptionProviderInterface {


  // Add common methods and abstract methods for your plugin type here.

}
